import './style.css'

import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

function createStar(scene) {

  const size = 10;
  const color = 0xffffff;
  const pos = new THREE.Vector3();
  pos.randomDirection();
  pos.multiplyScalar(5000);
  const mesh = new THREE.Mesh(
    new THREE.SphereGeometry(size, 3, 2),
    new THREE.MeshStandardMaterial({
      color: color
    })
  );

  mesh.position.set(...pos);
  scene.add(mesh);
}

function createMoon(planet) {

  const size = THREE.MathUtils.randFloat(0.1, 0.25);
  const color = Math.random() * 0xffffff;
  const s_pos = new THREE.Spherical(
    THREE.MathUtils.randFloat(2, 5),
    Math.PI/2,
    THREE.MathUtils.randFloat(0, 2*Math.PI)
  );
  const mesh = new THREE.Mesh(
    new THREE.SphereGeometry(size, 24, 24),
    new THREE.MeshStandardMaterial({
      color: color
    })
  );
  mesh.userData = {
    'mass': size/10,
    'vel': 0.01,
    's_pos': s_pos
  }

  mesh.position.setFromSpherical(s_pos);
  planet.add(mesh);
}

function createPlanet(sun) {

  const size = THREE.MathUtils.randFloat(0.5, 2);
  const color = Math.random() * 0xffffff;
  const s_pos = new THREE.Spherical(
    THREE.MathUtils.randFloat(50, 120),
    Math.PI/2,
    THREE.MathUtils.randFloat(0, 2*Math.PI)
  );
  const n_moons = THREE.MathUtils.randInt(0, 6);
  const mesh = new THREE.Mesh(
    new THREE.SphereGeometry(size, 24, 24),
    new THREE.MeshStandardMaterial({
      color: color
    })
  );
  mesh.position.setFromSpherical(s_pos);
  sun.add(mesh);
  mesh.userData = {
    'mass': size/10,
    'vel': 0.001,
    's_pos': s_pos
  }

  Array(n_moons).fill().forEach(() => {
    createMoon(mesh);
  });
}

function createSun(scene){

  const size = 10;
  const color = 0xffffee;
  const pos = new THREE.Vector3(0, 0, 0);
  const n_planets = THREE.MathUtils.randInt(5, 10);
  const mesh = new THREE.Mesh(
    new THREE.SphereGeometry(size, 32, 32),
    new THREE.MeshStandardMaterial({
      emissive: 0xeeeeaa,
      emissiveIntensity: 1.0,
      color: color,
      roughness: 1
    })
  );

  mesh.position.set(...pos);
  scene.add(mesh);

  Array(n_planets).fill().forEach(() => {
    createPlanet(mesh);
  });

  return mesh;
}

function createUniverse(scene){

  const sun = createSun(scene);

  Array(200).fill().forEach(() => {
    createStar(scene);
  });

  return sun;
}

function moveScene(sun){

  sun.children.forEach((planet) => {
    planet.userData.s_pos.theta += planet.userData.vel;
    planet.position.setFromSpherical(planet.userData.s_pos);
    planet.children.forEach((moon) => {
      moon.userData.s_pos.theta += moon.userData.vel;
      moon.position.setFromSpherical(moon.userData.s_pos);
    });
  });
}

function createCamera(){

  const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth/window.innerHeight,
    0.1,
    20000
  );

  camera.position.setX(0);
  camera.position.setY(20);
  camera.position.setZ(100);

  return camera;
}

function createRenderer(){

  const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#bg')
  });

  renderer.setPixelRatio(
    window.devicePixelRatio
  );
  renderer.setSize(
    window.innerWidth,
    window.innerHeight
  );

  return renderer;
}

function createControls(camera, renderer){

  const controls = new OrbitControls(camera, renderer.domElement);
  
  controls.enableDamping = true;
  controls.enableKeys = false;
  controls.enablePan = false;
  controls.minDistance = 50;
  controls.maxDistance = 200;

  return controls;
}

function onWindowResize(renderer, camera){

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate(renderer, scene, camera, controls, sun) {

  requestAnimationFrame(() => {
    animate(renderer, scene, camera, controls, sun)
  });

  controls.update();

  moveScene(sun);

  renderer.render(scene, camera);
}

function createScene(){

  const scene = new THREE.Scene();

  const pointLight = new THREE.PointLight(0xffffff);
  pointLight.position.set(0, 0, 0);

  const ambientLight = new THREE.AmbientLightProbe(0xffffff, 0.25);

  scene.add(pointLight, ambientLight);

  return scene;
}

function init(){
  
  const scene = createScene();
  const sun = createUniverse(scene);
  const camera = createCamera();
  const renderer = createRenderer();
  const controls = createControls(camera, renderer);
  animate(renderer, scene, camera, controls, sun);
  window.addEventListener(
    'resize',
    () => onWindowResize(renderer, camera),
    false
  );
}

init();
